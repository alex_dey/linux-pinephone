#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/i2c.h>
#include <linux/gpio.h>
#include <linux/interrupt.h>
#include <linux/irqreturn.h>
#include <linux/delay.h>
#include <linux/of_irq.h>
#include <linux/usb/typec.h>

#define ANX7688_REG_FUNCTION_OPTION	0x27
#define ANX7688_REG_IRQ_SOURCE_0	0x54
#define ANX7688_REG_IRQ_SOURCE_1	0x55
#define ANX7688_REG_IRQ_SOURCE_2	0x56

#define ANX7688_TCPC_REG_VENDOR_ID_0	0x00
#define ANX7688_TCPC_REG_VENDOR_ID_1	0x01

struct anx7688 {
	struct device *dev;
	struct i2c_client *client;
	struct i2c_client *client_tcpc;
	struct gpio_desc *gpio_poweron;
	struct gpio_desc *gpio_reset;
	struct gpio_desc *gpio_irq;
	struct gpio_desc *gpio_cabledet;
	int irq_plug;
	int irq_status;
	
	struct typec_port *port;
	struct typec_partner *partner;
	struct usb_pd_identity partner_identity;
};

static int anx7688_reg_read(struct anx7688 *anx7688, u8 reg_addr)
{
	int ret;
	ret = i2c_smbus_read_byte_data(anx7688->client, reg_addr);
	if (ret < 0)
		dev_printk(KERN_ERR, anx7688->dev, "i2c read failed addr 0x%x reg 0x%x, error %d\n",
				anx7688->client->addr, reg_addr, ret);
	return ret;
}

static int anx7688_tcpc_reg_read(struct anx7688 *anx7688, u8 reg_addr)
{
	int ret;
	ret = i2c_smbus_read_byte_data(anx7688->client_tcpc, reg_addr);
	if (ret < 0)
		dev_printk(KERN_ERR, anx7688->dev, "i2c read failed addr 0x%x reg 0x%x, error %d\n",
				anx7688->client_tcpc->addr, reg_addr, ret);
	return ret;
}

static int anx7688_reg_write(struct anx7688 *anx7688, u8 reg_addr, u8 value)
{
	int ret;
	ret = i2c_smbus_write_byte_data(anx7688->client, reg_addr, value);
	if (ret < 0)
		dev_printk(KERN_ERR, anx7688->dev, "i2c write failed addr 0x%x reg 0x%x, error %d\n",
				anx7688->client->addr, reg_addr, ret);
	return ret;
}

static int anx7688_tcpc_reg_write(struct anx7688 *anx7688, u8 reg_addr, u8 value)
{
	int ret;
	ret = i2c_smbus_write_byte_data(anx7688->client_tcpc, reg_addr, value);
	if (ret < 0)
		dev_printk(KERN_ERR, anx7688->dev, "i2c write failed addr 0x%x reg 0x%x, error %d\n",
				anx7688->client_tcpc->addr, reg_addr, ret);
	return ret;
}

static int anx7688_power_enable(struct anx7688 *anx7688)
{
	dev_printk(KERN_INFO, anx7688->dev, "ANX7688 power enable\n");
	gpiod_set_value(anx7688->gpio_poweron, 1);
	usleep_range(10000, 11000);
	gpiod_set_value(anx7688->gpio_reset, 1);
	usleep_range(12000, 13000);
	return 0;
}

static int anx7688_power_disable(struct anx7688 *anx7688)
{
	dev_printk(KERN_INFO, anx7688->dev, "ANX7688 power disable\n");
	gpiod_set_value(anx7688->gpio_reset, 0);
	usleep_range(1000, 1100);
	gpiod_set_value(anx7688->gpio_poweron, 0);
	usleep_range(1000, 1100);
	return 0;
}

static int anx7688_config(struct anx7688 *anx7688)
{
	// Enable automatic PD mode and VBUS ADC
	anx7688_reg_write(anx7688, ANX7688_REG_FUNCTION_OPTION, 0b00000011);
	return 0;
}

static int anx7688_connect(struct anx7688 *anx7688)
{
	struct typec_partner_desc desc;

	dev_printk(KERN_INFO, anx7688->dev, "Cable inserted\n");
	anx7688_power_enable(anx7688);
	anx7688_config(anx7688);

	desc.usb_pd = false;
	desc.accessory = TYPEC_ACCESSORY_NONE;
	desc.identity = NULL;

	anx7688->partner = typec_register_partner(anx7688->port, &desc);
	if (IS_ERR(anx7688->partner))
		return PTR_ERR(anx7688->partner);

	if (desc.identity)
		typec_partner_set_identity(anx7688->partner);

	return 0;
}

static int anx7688_disconnect(struct anx7688 *anx7688)
{
	dev_printk(KERN_INFO, anx7688->dev, "Cable removed\n");

	if (!IS_ERR(anx7688->partner))
		typec_unregister_partner(anx7688->partner);

	anx7688->partner = NULL;
	
	typec_set_pwr_opmode(anx7688->port, TYPEC_PWR_MODE_USB);
	typec_set_pwr_role(anx7688->port, TYPEC_SINK);
	typec_set_data_role(anx7688->port, TYPEC_DEVICE);
	anx7688_power_disable(anx7688);

	return 0;
}

static irqreturn_t anx7688_irq_plug_handler(int irq, void *data)
{
	struct i2c_client *client = to_i2c_client(data);
	struct anx7688 *anx7688 = i2c_get_clientdata(client);
	int value;

	value = gpiod_get_value(anx7688->gpio_cabledet);
	if(value){
		anx7688_connect(anx7688);
	}else{
		anx7688_disconnect(anx7688);
	}

	return IRQ_HANDLED;
}

static irqreturn_t anx7688_irq_status_handler(int irq, void *data)
{
	struct i2c_client *client = to_i2c_client(data);
	struct anx7688 *anx7688 = i2c_get_clientdata(client);
	int source0;
	int source1;
	int source2;

	dev_printk(KERN_INFO, anx7688->dev, "status interrupt received\n");
	source0 = anx7688_reg_read(anx7688, ANX7688_REG_IRQ_SOURCE_0);
	source1 = anx7688_reg_read(anx7688, ANX7688_REG_IRQ_SOURCE_1);
	source2 = anx7688_reg_read(anx7688, ANX7688_REG_IRQ_SOURCE_2);
	dev_printk(KERN_INFO, anx7688->dev, "source is %d %d %d\n", source0, source1, source2);

	// Clear IRQ bits
	anx7688_reg_write(anx7688, ANX7688_REG_IRQ_SOURCE_0, 0);
	anx7688_reg_write(anx7688, ANX7688_REG_IRQ_SOURCE_1, 0);
	anx7688_reg_write(anx7688, ANX7688_REG_IRQ_SOURCE_2, 0);

	return IRQ_HANDLED;
}

static int anx7688_dr_set(struct typec_port *port, enum typec_data_role role)
{
	printk(KERN_WARNING "data role set\n");
	return 0;
}

static int anx7688_pr_set(struct typec_port *port, enum typec_role role)
{
	printk(KERN_WARNING "power role set\n");
	return 0;
}

static const struct typec_operations anx7688_typec_ops = {
	.dr_set = anx7688_dr_set,
	.pr_set = anx7688_pr_set,
};

static int anx7688_i2c_probe(struct i2c_client *client,
			     const struct i2c_device_id *id)
{
	struct anx7688 *anx7688;
	struct device *dev = &client->dev;
	struct typec_capability typec_cap = { };
	int typec_revision;
	int vid;
	int ret = 0;
	
	anx7688 = devm_kzalloc(&client->dev, sizeof(struct anx7688), GFP_KERNEL);
	if (!anx7688)
		return -ENOMEM;

	i2c_set_clientdata(client, anx7688);
	anx7688->client = client;
	anx7688->dev = &client->dev;

	// Register the TCPC i2c interface as second interface (0x58)
	anx7688->client_tcpc = i2c_new_dummy_device(client->adapter, 0x2c);

	dev_printk(KERN_INFO, dev, "=== ANX7688 i2c probe ===\n");

	anx7688->gpio_poweron = devm_gpiod_get(&client->dev, "enable", GPIOD_OUT_LOW);
	if(IS_ERR(anx7688->gpio_poweron)) {
		dev_printk(KERN_ERR, dev, "Could not get poweron gpio\n");
		return PTR_ERR(anx7688->gpio_poweron);
	}
	anx7688->gpio_reset = devm_gpiod_get(&client->dev, "reset", GPIOD_OUT_LOW);
	if(IS_ERR(anx7688->gpio_reset)) {
		dev_printk(KERN_ERR, dev, "Could not get reset gpio\n");
		return PTR_ERR(anx7688->gpio_reset);
	}
	anx7688->gpio_cabledet = devm_gpiod_get(&client->dev, "cabledet", GPIOD_IN);
	if(IS_ERR(anx7688->gpio_cabledet)) {
		dev_printk(KERN_ERR, dev, "Could not get cabledet gpio\n");
		return PTR_ERR(anx7688->gpio_cabledet);
	}

	anx7688_power_enable(anx7688);
	vid = anx7688_tcpc_reg_read(anx7688, ANX7688_TCPC_REG_VENDOR_ID_0);
	vid |= anx7688_tcpc_reg_read(anx7688, ANX7688_TCPC_REG_VENDOR_ID_1) >> 8;
	dev_printk(KERN_INFO, dev, "Chip vendor id is %x\n", vid);
	typec_revision = anx7688_reg_read(anx7688, 0x06);
	anx7688_power_disable(anx7688);

	typec_cap.revision = USB_TYPEC_REV_1_2;
	typec_cap.pd_revision = 0x200;
	typec_cap.prefer_role = TYPEC_NO_PREFERRED_ROLE;
	typec_cap.ops = &anx7688_typec_ops;
	typec_cap.type = TYPEC_PORT_DRP;
	typec_cap.data = TYPEC_PORT_DRD;
	
	anx7688->port = typec_register_port(&client->dev, &typec_cap);
	if (IS_ERR(anx7688->port)) {
		dev_err(dev, "Could not register type-c port\n");
		return PTR_ERR(anx7688->port);
	}

	anx7688->irq_plug = of_irq_get(dev->of_node, 0);
	anx7688->irq_status = of_irq_get(dev->of_node, 1);

	ret = devm_request_threaded_irq(&client->dev, anx7688->irq_plug, NULL, anx7688_irq_plug_handler, IRQF_ONESHOT, "anx7688plug", &client->dev);
	if (ret < 0) {
		dev_err(dev, "Could not register cable detection irq: %d\n", ret);
		goto error;
	}
	ret = devm_request_threaded_irq(&client->dev, anx7688->irq_status, NULL, anx7688_irq_status_handler, IRQF_ONESHOT, "anx7688status", &client->dev);
	if (ret < 0) {
		dev_err(dev, "Could not register status update irq: %d\n", ret);
		goto error;
	}

error:
	if (ret) {
		typec_unregister_port(anx7688->port);
	}

	return ret;
}

static int anx7688_i2c_remove(struct i2c_client *client)
{
	struct anx7688 *anx7688;
	printk(KERN_INFO "ANX7688 i2c remove\n");
	anx7688 = i2c_get_clientdata(client);


	anx7688_power_disable(anx7688);
	devm_free_irq(anx7688->dev, anx7688->irq_plug, anx7688->dev);
	devm_free_irq(anx7688->dev, anx7688->irq_status, anx7688->dev);
	i2c_unregister_device(anx7688->client_tcpc);
	return 0;
}

static const struct i2c_device_id anx7688_id[] = {
	{"anx7688", 0},
	{}
};

MODULE_DEVICE_TABLE(i2c, anx7688_id);

#ifdef CONFIG_OF
static struct of_device_id anx_match_table[] = {
	{.compatible = "analogix,anx7688",},
	{},
};
#endif

static struct i2c_driver anx7688_driver = {
	.driver = {
		.name = "anx7688",
#ifdef CONFIG_OF
		.of_match_table = anx_match_table,
#endif
	},
	.probe = anx7688_i2c_probe,
	.remove = anx7688_i2c_remove,
	.id_table = anx7688_id,
};

module_i2c_driver(anx7688_driver);

MODULE_LICENSE("GPL");
MODULE_AUTHOR("Martijn Braam <martijn@brixit.nl>");
MODULE_DESCRIPTION("Analogix ANX7688 USB-C DisplayPort bridge");
MODULE_VERSION("0.01");

